# Maximauve_
# 19/10/2020
## Affichage des informations globales de son système.
 
#                           ___  ____    ___        __       
#                          / _ \/ ___|  |_ _|_ __  / _| ___  
#                         | | | \___ \   | || '_ \| |_ / _ \ 
#                         | |_| |___) |  | || | | |  _| (_) |
#                          \___/|____/  |___|_| |_|_|  \___/             

Clear-Host
Write-Host -NoNewline "Computer's name: "
$system_name = $([system.environment]::MachineName)
$system_name


Write-Host -NoNewline "IP adress: "
$ip_info = $(Get-NetIPAddress -InterfaceAlias "Wi-Fi" -AddressFamily "IPv4")
$ip_info.IPAddress

Write-Host -NoNewline "Os Name: "
$os_name = $(Get-ComputerInfo -Property "OsName")
$os_name.OsName

Write-Host -NoNewline "Os version: "
$os_version = $(Get-ComputerInfo -Property "OsVersion")
$os_version.OsVersion

$uptime = $(Get-ciminstance win32_operatingsystem | Select-Object -ExpandProperty lastbootuptime)
Write-Output "Last ignition: $uptime"

$UpToDate = $(Get-Command -Module WindowsUpdateProvider Start-WUScan -SearchCriteria "IsInstalled=0 AND IsHidden=0 AND IsAssigned=1")
if ($UpToDate) {
	Write-Output "Os up-to-date: True"
}
else {
	Write-Output "Os up-to-date: False"
}
Write-Output `n

Write-Host -NoNewline "Used physical memory: "
$TotalRAM = $(Get-ComputerInfo -Property "OsTotalVisibleMemorySize")
$FreeRAM = $(Get-ComputerInfo -Property "OsFreePhysicalMemory")
$UsedRAM = $TotalRAM.OsTotalVisibleMemorySize - $FreeRAM.OsFreePhysicalMemory
$UsedRAM / 1MB

Write-Host -NoNewline "Free physical memory: "
$FreeRAM.OsFreePhysicalMemory / 1MB
Write-Output `n

Write-Host -NoNewline "Used space disk: "
$TotalDisk = $(get-WmiObject win32_logicaldisk | Select-Object "Size")
$FreeDisk = $(get-WmiObject win32_logicaldisk | Select-Object "FreeSpace")
$UsedDisk = $TotalDisk.Size - $FreeDisk.FreeSpace
$UsedDisk / 1GB

Write-Host -NoNewline "Free space disk: "
$FreeDisk.FreeSpace / 1GB
Write-Output `n

#                            _   _ ____  _____ ____  
#                           | | | / ___|| ____|  _ \ 
#                           | | | \___ \|  _| | |_) |
#                           | |_| |___) | |___|  _ < 
#                            \___/|____/|_____|_| \_\

Write-Output "User List: "
$UserList = $(Get-LocalUser | Select-Object Name)
$UserList.Name
Write-Output `n

#                             ____  _             
#                            |  _ \(_)_ __   __ _ 
#                            | |_) | | '_ \ / _` |
#                            |  __/| | | | | (_| |
#                            |_|   |_|_| |_|\__, |
#                                           |___/ 

$Ping1 = $(Test-Connection 8.8.8.8 -count 1 | Select-Object ResponseTime)
$Ping2 = $(Test-Connection 8.8.8.8 -count 1 | Select-Object ResponseTime)
$Ping3 = $(Test-Connection 8.8.8.8 -count 1 | Select-Object ResponseTime)
$Ping4 = $(Test-Connection 8.8.8.8 -count 1 | Select-Object ResponseTime)
$Ping5 = $(Test-Connection 8.8.8.8 -count 1 | Select-Object ResponseTime)
$Average_Ping = [math]::Round(($Ping1.ResponseTime + $Ping2.ResponseTime + $Ping3.ResponseTime + $Ping4.ResponseTime + $Ping5.ResponseTime) / 5)
Write-Output "Average ping to '8.8.8.8': $Average_Ping ms" 
Write-Output `n
