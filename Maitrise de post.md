# Maitrise de post

## I. Self footprinting

### Host OS

```
PS C:\Users\maxmo\GitLab\workstation> systeminfo
[...]
Nom de l’hôte:                              PC-DE-MAXIMAUVE
Nom du système d’exploitation:              Microsoft Windows 10 Famille
Version du système:                         10.0.18363 N/A version 18363
Type du système:                            x64-based PC
Mémoire physique totale:                    16 182 Mo
[...]
```

```
PS C:\Users\maxmo\GitLab\workstation> Get-WmiObject win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosize

Manufacturer Banklabel Configuredclockspeed Devicelocator    Capacity Serialnumber
------------ --------- -------------------- -------------    -------- ------------
Samsung      BANK 0                    2133 ChannelA-DIMM0 8589934592 00000000
Samsung      BANK 2                    2133 ChannelB-DIMM0 8589934592 00000000
```

### Devices

#### CPU

```
PS C:\Users\maxmo\GitLab\workstation> Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*

Name                                      NumberOfCores NumberOfEnabledCore NumberOfLogicalProcessors
----                                      ------------- ------------------- -------------------------
Intel(R) Core(TM) i7-10510U CPU @ 1.80GHz             4                   4                         8
```

- Intel(R) Core(TM) : Marque
- i7 : Caractéristiques de la Marque
- 10510 :
- - 10 = Génération du procésseur
- - 510 = Unité de gestion des informations
- U : Complément d'information
- - U = Puissance mobile efficace

#### GPU

```
PS C:\Users\maxmo\GitLab\workstation> Get-WmiObject -Class Win32_VideoController | Select-Object -Property Name, Number*

Name                                      NumberOfColorPlanes NumberOfVideoPages
----                                      ------------------- ------------------
NVIDIA GeForce GTX 1650 with Max-Q Design
Intel(R) UHD Graphics
```

---

#### Disque Dur

```
PS C:\Users\maxmo\GitLab\workstation> diskpart

    -> DISKPART> sel disk 0

        -> DISKPART> detail disc

INTEL SSDPEKNW512G8

[...]

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     C   OS           NTFS   Partition    475 G   Sain       Démarrag
  Volume 1         SYSTEM       FAT32  Partition    260 M   Sain       Système
```

### User

```
PS C:\Users\maxmo\GitLab\workstation> net user

comptes d’utilisateurs de \\PC-DE-MAXIMAUVE

-------------------------------------------------------------------------------
Administrateur           DefaultAccount           Invité
maxmo                    WDAGUtilityAccount
La commande s’est terminée correctement.
```

### Processus

```
PS C:\Users\maxmo\GitLab\workstation> tasklist

Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
========================= ======== ================ =========== ============
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0     8 444 Ko
Secure System                   72 Services                   0    41 352 Ko
Registry                       128 Services                   0    71 496 Ko
smss.exe                       716 Services                   0     1 116 Ko
csrss.exe                      936 Services                   0     5 000 Ko
wininit.exe                     96 Services                   0     5 804 Ko
services.exe                   888 Services                   0     9 752 Ko
LsaIso.exe                     960 Services                   0     3 092 Ko
lsass.exe                      940 Services                   0    19 348 Ko
svchost.exe                   1144 Services                   0     3 652 Ko
fontdrvhost.exe               1168 Services                   0     2 520 Ko
svchost.exe                   1176 Services                   0    34 524 Ko
WUDFHost.exe                  1244 Services                   0    18 692 Ko
svchost.exe                   1276 Services                   0    18 612 Ko
svchost.exe                   1340 Services                   0     8 108 Ko
WUDFHost.exe                  1552 Services                   0     5 980 Ko
svchost.exe                   1696 Services                   0     6 876 Ko
svchost.exe                   1780 Services                   0    10 112 Ko
svchost.exe                   1832 Services                   0     4 700 Ko
svchost.exe                   1840 Services                   0    11 968 Ko
svchost.exe                   1856 Services                   0     7 916 Ko
svchost.exe                   1868 Services                   0    10 504 Ko
svchost.exe                   1936 Services                   0     9 864 Ko
svchost.exe                   1968 Services                   0     9 648 Ko
svchost.exe                   2032 Services                   0    11 272 Ko
svchost.exe                   1540 Services                   0    15 532 Ko
svchost.exe                   2068 Services                   0     8 144 Ko
svchost.exe                   2176 Services                   0     5 516 Ko
svchost.exe                   2244 Services                   0     7 360 Ko
svchost.exe                   2292 Services                   0     7 428 Ko
svchost.exe                   2320 Services                   0     7 940 Ko
svchost.exe                   2464 Services                   0    12 288 Ko
svchost.exe                   2484 Services                   0     7 836 Ko
svchost.exe                   2524 Services                   0     9 160 Ko
svchost.exe                   2560 Services                   0    11 508 Ko
svchost.exe                   2660 Services                   0    16 504 Ko
svchost.exe                   2716 Services                   0    15 672 Ko
svchost.exe                   2768 Services                   0    14 860 Ko
svchost.exe                   2792 Services                   0     9 952 Ko
svchost.exe                   2940 Services                   0     9 712 Ko
svchost.exe                   2984 Services                   0     8 508 Ko
NVDisplay.Container.exe       2404 Services                   0    14 640 Ko
svchost.exe                   3236 Services                   0    13 388 Ko
svchost.exe                   3240 Services                   0     5 248 Ko
svchost.exe                   3268 Services                   0     6 964 Ko
Memory Compression            3348 Services                   0   331 572 Ko
svchost.exe                   3408 Services                   0    20 864 Ko
svchost.exe                   3432 Services                   0     8 856 Ko
igfxCUIService.exe            3520 Services                   0     7 684 Ko
svchost.exe                   3576 Services                   0     7 640 Ko
svchost.exe                   3584 Services                   0     8 200 Ko
WmiPrvSE.exe                  3688 Services                   0    15 860 Ko
svchost.exe                   3744 Services                   0    13 852 Ko
svchost.exe                   3892 Services                   0    10 748 Ko
svchost.exe                   4016 Services                   0     5 916 Ko
svchost.exe                   4072 Services                   0    19 192 Ko
AsusOptimization.exe          3144 Services                   0     6 244 Ko
svchost.exe                   3492 Services                   0    14 256 Ko
wlanext.exe                   3924 Services                   0     6 156 Ko
conhost.exe                   4128 Services                   0     4 752 Ko
svchost.exe                   4236 Services                   0     5 492 Ko
Intel_PIE_Service.exe         4332 Services                   0     7 308 Ko
svchost.exe                   4408 Services                   0     6 080 Ko
spoolsv.exe                   4484 Services                   0    15 240 Ko
svchost.exe                   4548 Services                   0   186 692 Ko
svchost.exe                   4580 Services                   0    14 184 Ko
taskhostw.exe                 4728 Services                   0    45 904 Ko
svchost.exe                   4892 Services                   0     7 252 Ko
svchost.exe                   4900 Services                   0     6 488 Ko
svchost.exe                   5008 Services                   0     4 408 Ko
svchost.exe                   5016 Services                   0     6 284 Ko
svchost.exe                   5096 Services                   0     7 136 Ko
AsusLinkNearExt.exe           4228 Services                   0     2 912 Ko
AsusLinkNear.exe              4268 Services                   0     9 288 Ko
AsusInitialService.exe        4304 Services                   0     9 972 Ko
AsusSystemDiagnosis.exe       4404 Services                   0     7 996 Ko
AsusLinkRemote.exe            4480 Services                   0    11 788 Ko
svchost.exe                   4316 Services                   0    20 420 Ko
AsusSoftwareManager.exe       4120 Services                   0    20 500 Ko
IntelCpHDCPSvc.exe            4276 Services                   0     6 596 Ko
AsusSystemAnalysis.exe        4600 Services                   0    13 696 Ko
svchost.exe                   4748 Services                   0    40 936 Ko
svchost.exe                   4648 Services                   0    29 644 Ko
ibtsiva.exe                   5036 Services                   0     4 180 Ko
ICEsoundService64.exe         5124 Services                   0     6 104 Ko
OneApp.IGCC.WinService.ex     5136 Services                   0    25 848 Ko
esif_uf.exe                   5216 Services                   0     5 384 Ko
IntelAudioService.exe         5232 Services                   0    20 376 Ko
svchost.exe                   5240 Services                   0    13 056 Ko
NortonSecurity.exe            5320 Services                   0    48 708 Ko
nsWscSvc.exe                  5376 Services                   0     8 288 Ko
RstMwService.exe              5448 Services                   0     5 956 Ko
svchost.exe                   5456 Services                   0     5 460 Ko
RtkAudUService64.exe          5492 Services                   0     8 604 Ko
svchost.exe                   5508 Services                   0     7 392 Ko
svchost.exe                   5540 Services                   0     4 916 Ko
svchost.exe                   5576 Services                   0    20 512 Ko
svchost.exe                   5716 Services                   0     5 220 Ko
jhi_service.exe               5876 Services                   0     4 804 Ko
IntelCpHeciSvc.exe            6148 Services                   0     6 004 Ko
svchost.exe                   6480 Services                   0     7 880 Ko
unsecapp.exe                  6756 Services                   0     8 760 Ko
svchost.exe                   6780 Services                   0    10 976 Ko
dllhost.exe                   8024 Services                   0    10 200 Ko
PresentationFontCache.exe     7356 Services                   0    16 264 Ko
svchost.exe                   8312 Services                   0    17 192 Ko
svchost.exe                   8864 Services                   0     7 700 Ko
svchost.exe                   8996 Services                   0     8 784 Ko
svchost.exe                   9140 Services                   0    20 096 Ko
GoogleCrashHandler.exe        9028 Services                   0     1 356 Ko
GoogleCrashHandler64.exe     10052 Services                   0     1 076 Ko
svchost.exe                  12108 Services                   0    20 440 Ko
svchost.exe                  11656 Services                   0     9 916 Ko
svchost.exe                   7384 Services                   0    14 700 Ko
svchost.exe                   4196 Services                   0    16 684 Ko
svchost.exe                  12132 Services                   0    11 188 Ko
SgrmBroker.exe                9424 Services                   0     6 436 Ko
svchost.exe                   9700 Services                   0    10 224 Ko
SecurityHealthService.exe    10036 Services                   0    13 680 Ko
svchost.exe                  12996 Services                   0     8 020 Ko
svchost.exe                   9480 Services                   0    18 852 Ko
svchost.exe                  12140 Services                   0     9 544 Ko
svchost.exe                  11676 Services                   0    17 852 Ko
svchost.exe                   7328 Services                   0     6 812 Ko
svchost.exe                  14236 Services                   0     9 824 Ko
OfficeClickToRun.exe         16028 Services                   0    52 068 Ko
AppVShNotify.exe             19216 Services                   0    11 092 Ko
SearchIndexer.exe             3740 Services                   0    44 736 Ko
csrss.exe                    19704 Console                    2     6 244 Ko
winlogon.exe                  2408 Console                    2     9 804 Ko
fontdrvhost.exe              16516 Console                    2     7 088 Ko
dwm.exe                      20048 Console                    2    92 584 Ko
AsusScreenPadService.exe     15400 Console                    2    22 736 Ko
unsecapp.exe                  4392 Console                    2    11 068 Ko
NVDisplay.Container.exe      11384 Console                    2    33 460 Ko
AsusOptimizationStartupTa     5264 Console                    2    13 480 Ko
AsusFeatureService.exe        4040 Console                    2    11 384 Ko
AsusLinkToScreenXpert.exe     3040 Console                    2     6 716 Ko
sihost.exe                    7988 Console                    2    28 440 Ko
svchost.exe                  16964 Console                    2    26 880 Ko
svchost.exe                  14280 Console                    2     7 868 Ko
igfxEM.exe                    8528 Console                    2    23 260 Ko
svchost.exe                  13872 Console                    2    35 248 Ko
taskhostw.exe                12556 Console                    2    20 260 Ko
explorer.exe                 13468 Console                    2   167 356 Ko
svchost.exe                  18000 Console                    2    24 340 Ko
StartMenuExperienceHost.e    17704 Console                    2    71 376 Ko
RuntimeBroker.exe             7460 Console                    2    24 676 Ko
SearchUI.exe                 18932 Console                    2   273 544 Ko
RuntimeBroker.exe            11216 Console                    2    48 700 Ko
YourPhone.exe                 7828 Console                    2    17 816 Ko
SettingSyncHost.exe          17736 Console                    2     7 476 Ko
ctfmon.exe                   14576 Console                    2    14 436 Ko
TabTip.exe                   15276 Console                    2    14 644 Ko
RuntimeBroker.exe              300 Console                    2    24 524 Ko
RuntimeBroker.exe            15024 Console                    2    13 316 Ko
AsusScreenPad.exe            15832 Console                    2   116 608 Ko
SecurityHealthSystray.exe    11360 Console                    2     8 404 Ko
vgtray.exe                   15328 Console                    2     6 652 Ko
OneDrive.exe                 15668 Console                    2    90 140 Ko
Discord.exe                  18512 Console                    2    89 688 Ko
Discord.exe                  17744 Console                    2   170 608 Ko
Discord.exe                  14036 Console                    2    28 356 Ko
EpicGamesLauncher.exe        20328 Console                    2    70 056 Ko
UnrealCEFSubProcess.exe      14908 Console                    2    30 544 Ko
jusched.exe                  11868 Console                    2    14 048 Ko
ShellExperienceHost.exe       9084 Console                    2    77 956 Ko
RuntimeBroker.exe             6456 Console                    2    26 456 Ko
NortonSecurity.exe            9508 Console                    2     9 948 Ko
Discord.exe                   7984 Console                    2    13 012 Ko
Discord.exe                  19992 Console                    2   347 596 Ko
RtkAudUService64.exe         10572 Console                    2     1 536 Ko
Discord.exe                  11812 Console                    2    18 664 Ko
AsusOSD.exe                  14716 Console                    2    12 360 Ko
CompPkgSrv.exe               12060 Console                    2     8 256 Ko
ApplicationFrameHost.exe     14788 Console                    2    40 864 Ko
svchost.exe                  12308 Console                    2    20 180 Ko
Calculator.exe                5212 Console                    2       512 Ko
commsapps.exe                 7124 Console                    2       704 Ko
RuntimeBroker.exe             5572 Console                    2    23 812 Ko
HxTsr.exe                    16000 Console                    2    29 036 Ko
HxAccounts.exe                4224 Console                    2    56 048 Ko
rundll32.exe                 19648 Console                    2     8 792 Ko
chrome.exe                    5152 Console                    2   323 264 Ko
chrome.exe                   10644 Console                    2     6 456 Ko
chrome.exe                    7924 Console                    2   256 572 Ko
chrome.exe                    3604 Console                    2    47 860 Ko
chrome.exe                   19124 Console                    2    14 524 Ko
chrome.exe                   16976 Console                    2   140 052 Ko
chrome.exe                    6604 Console                    2    55 996 Ko
chrome.exe                   18236 Console                    2    84 940 Ko
chrome.exe                    6228 Console                    2    31 504 Ko
chrome.exe                    6096 Console                    2    16 376 Ko
LockApp.exe                  18660 Console                    2    54 200 Ko
RuntimeBroker.exe            17900 Console                    2    30 744 Ko
svchost.exe                   6580 Services                   0     6 520 Ko
svchost.exe                    924 Services                   0     8 632 Ko
dasHost.exe                   7376 Services                   0    10 200 Ko
AsusSoftwareManagerAgent.    17556 Console                    2    36 520 Ko
WindowsInternal.Composabl    11448 Console                    2    44 672 Ko
WinStore.App.exe              5588 Console                    2       544 Ko
RuntimeBroker.exe            16784 Console                    2    19 212 Ko
svchost.exe                  10508 Services                   0     7 488 Ko
svchost.exe                  11648 Services                   0     6 416 Ko
FileCoAuth.exe                6808 Console                    2    20 404 Ko
chrome.exe                   14804 Console                    2    99 952 Ko
svchost.exe                  18800 Services                   0     9 796 Ko
svchost.exe                   3544 Services                   0    10 180 Ko
svchost.exe                  17604 Console                    2    10 548 Ko
conhost.exe                   7800 Console                    2     5 964 Ko
audiodg.exe                  15788 Services                   0    22 040 Ko
chrome.exe                   16920 Console                    2   147 216 Ko
chrome.exe                    2616 Console                    2    48 948 Ko
SystemSettings.exe           15220 Console                    2       452 Ko
chrome.exe                    8124 Console                    2   119 140 Ko
chrome.exe                   15672 Console                    2    21 468 Ko
dllhost.exe                  18352 Console                    2    16 736 Ko
svchost.exe                   9532 Services                   0     5 776 Ko
vds.exe                      19024 Services                   0    13 160 Ko
powershell.exe               15844 Console                    2    77 516 Ko
conhost.exe                   9488 Console                    2    16 316 Ko
tasklist.exe                  1864 Console                    2     9 892 Ko
WmiPrvSE.exe                  4540 Services                   0     8 816 Ko
```

- `svchost.exe` -> Son rôle principal consiste à charger des bibliothèques de liens dynamiques
- `dllhost.exe` -> Son rôle est de gérer les librairies dynamiques (DLL) basées sur des objets COM
- `dwm.exe` -> **D**esktop **W**indows **M**anager, il sert à gérer les affichages du bureau et des fenêtres de Windows.
- `vsd.exe` -> **V**irtual **D**isk **S**ervice, c'est un gestionnaire de configuration de stockage pour Windows.
- `wininit.exe` -> **Win**dows **Init**ialization, permet l'initialisation de Windows.

### Network

#### Carte Réseau

```
PS C:\Users\maxmo\GitLab\workstation> Get-NetAdapter
Name                      InterfaceDescription                    ifIndex Status       MacAddress
----                      --------------------                    ------- ------       ----------
Connexion réseau Bluet... Bluetooth Device (Personal Area Netw...      18 Disconnected C8-58-C0-27...
VirtualBox Host-Only N... VirtualBox Host-Only Ethernet Adapter        10 Up           0A-00-27-00...
Wi-Fi                     Intel(R) Wi-Fi 6 AX201 160MHz                 2 Up           C8-58-C0-27...
```

- `Bluetooth Device (Personal Area Netw...` -> C'est la carte permettant une connexion Bluetooth.
- `VirtualBox Host-Only Ethernet Adapter` -> C'est une carte permettant d'accéder à une VM depuis un réseau.
- `Intel(R) Wi-Fi 6 AX201 160MHz` -> C'est la carte permettant de se connecter en Wi-Fi (sans fil).

#### Ports TCP / UDP

```
PS C:\Windows\system32> netstat -a -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            PC-de-Maximauve:0      LISTENING
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:808            PC-de-Maximauve:0      LISTENING
 [OneApp.IGCC.WinService.exe]
  TCP    0.0.0.0:5040           PC-de-Maximauve:0      LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:5357           PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:9001           PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49664          PC-de-Maximauve:0      LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          PC-de-Maximauve:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          PC-de-Maximauve:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          PC-de-Maximauve:0      LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49670          PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49887          PC-de-Maximauve:0      LISTENING
 [NortonSecurity.exe]
  TCP    0.0.0.0:49888          PC-de-Maximauve:0      LISTENING
 [NortonSecurity.exe]
  TCP    10.33.2.91:139         PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.2.91:49419       40.67.251.132:https    ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    10.33.2.91:55295       40.67.251.132:https    ESTABLISHED
 [OneDrive.exe]
  TCP    10.33.2.91:56681       wq-in-f188:5228        ESTABLISHED
 [chrome.exe]
  TCP    10.33.2.91:56683       162.159.135.234:https  ESTABLISHED
 [Discord.exe]
  TCP    10.33.2.91:56689       ec2-18-179-138-200:https  ESTABLISHED
 [chrome.exe]
  TCP    10.33.2.91:56844       152.199.19.161:https   CLOSE_WAIT
 [SearchUI.exe]
  TCP    10.33.2.91:56915       ec2-3-213-18-157:https  ESTABLISHED
 [chrome.exe]
  TCP    10.33.2.91:56916       ec2-3-213-18-157:https  ESTABLISHED
 [chrome.exe]
  TCP    10.33.2.91:56917       ec2-52-71-23-32:https  ESTABLISHED
 [EpicGamesLauncher.exe]
  TCP    127.0.0.1:6463         PC-de-Maximauve:0      LISTENING
 [Discord.exe]
  TCP    192.168.56.1:139       PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:135               PC-de-Maximauve:0      LISTENING
  RpcSs
 [svchost.exe]
  TCP    [::]:445               PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:808               PC-de-Maximauve:0      LISTENING
 [OneApp.IGCC.WinService.exe]
  TCP    [::]:5357              PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:9001              PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49664             PC-de-Maximauve:0      LISTENING
 [lsass.exe]
  TCP    [::]:49665             PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             PC-de-Maximauve:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    [::]:49667             PC-de-Maximauve:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    [::]:49668             PC-de-Maximauve:0      LISTENING
 [spoolsv.exe]
  TCP    [::]:49670             PC-de-Maximauve:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49888             PC-de-Maximauve:0      LISTENING
 [NortonSecurity.exe]
  TCP    [::1]:49669            PC-de-Maximauve:0      LISTENING
 [jhi_service.exe]
  UDP    0.0.0.0:500            *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5355           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:6666           *:*
 [EpicGamesLauncher.exe]
  UDP    0.0.0.0:55623          *:*
 [EpicGamesLauncher.exe]
  UDP    0.0.0.0:59899          *:*
  FDResPub
 [svchost.exe]
  UDP    10.33.2.91:137         *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.2.91:138         *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.2.91:1900        *:*
  SSDPSRV
 [svchost.exe]
  UDP    10.33.2.91:61194       *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:1900         *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:61195        *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:63172        *:*
  NlaSvc
 [svchost.exe]
  UDP    127.0.0.1:64710        *:*
  iphlpsvc
 [svchost.exe]
  UDP    192.168.56.1:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.56.1:61193     *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::]:500               *:*
  IKEEXT
 [svchost.exe]
  UDP    [::]:3702              *:*
  FDResPub
 [svchost.exe]
  UDP    [::]:3702              *:*
  FDResPub
 [svchost.exe]
  UDP    [::]:4500              *:*
  IKEEXT
 [svchost.exe]
  UDP    [::]:5353              *:*
 [chrome.exe]
  UDP    [::]:5353              *:*
 [chrome.exe]
  UDP    [::]:5353              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:5355              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:59900             *:*
  FDResPub
 [svchost.exe]
  UDP    [::1]:1900             *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:61192            *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::399d:de64:669e:a931%10]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::399d:de64:669e:a931%10]:61190  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::c581:a752:f14:64c9%2]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::c581:a752:f14:64c9%2]:61191  *:*
  SSDPSRV
 [svchost.exe]
```

## III. Gestion de softs

C'est plus pratique d'avoir un gestionnaire de paquets pour plusieurs raisons:

- Le téléchargement de driver est plus rapide et sécurisé car il passe directement par le serveur de téléchargement des drivers, et non par un moeteur de recherche.
- Notre identité est protégée, car on ne passe pas par un moteur de recherche
- Les paquets ne peuvent pas posséder de virus.

Voici la liste de mes paquets installés:

```
PS C:\Users\maxmo> choco list
Chocolatey v0.10.15
PS C:\Users\maxmo>
```

## IV. Machine Virtuelle

Après avoir bien suivi les étapes d'installation de la machine virtuelle, je suis parvenu à échanger des fichiers entre mon ordinateur et ma machine virtuelle.

Depuis mon ordinateur:

```
PS C:\Users\maxmo\OneDrive\Bureau\share> ls


   Répertoire : C:\Users\maxmo\OneDrive\Bureau\share


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a---l       15/11/2020     14:39             13 test.txt


PS C:\Users\maxmo\OneDrive\Bureau\share>
```

Et ma machine virtuelle:

```
[root@localhost partage]# ls
test.txt
[root@localhost partage]#
```
