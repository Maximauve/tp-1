#Maximauve_
#10/11/2020
#Script permettant de verouiller ou d'éteindre son pc après tant de secondes.

if ($args[0] = "lock") {
  if ($args[1] -is [int]) {
    $nb = $args[1]
    Write-Output "Locking screen in $nb seconds"
    Start-Sleep -Seconds $nb
    $lockscreen = $(rundll32.exe user32.dll, LockWorkStation)
    $lockscreen
  }
  elseif ($null -eq $args[1]) {
    Write-Output "Missing seconds."
  }
  else {
    Write-Output "Seconds unidentified, perhaps you misstyped."
  }
}
elseif ($args[0] = "shutdown") {
  if ($args[1] -is [int]) {
    $nb = $args[1]
    Write-Output "Shutting down in $nb seconds"
    Start-Sleep -Seconds $nb
    $shutdown = $(Stop-Computer)
    $shutdown
  }
  elseif ($null -eq $args[1]) {
    Write-Output "Missing seconds."
  }
  else {
    Write-Output "Seconds unidentified, perhaps you misstyped."
  }
}
elseif ($null -eq $args[0]) {
  Write-Output "Missing command."
}
else {
  Write-Output "Command not found. Please try <lock> or <shutdown>."
}